﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Function.IntegrationsTest
{
    [Collection(nameof(FunctionTestCollection))]
    public class FunctionWelcomeTest
    {
        private FunctionTestFixture testFixture;
        private HttpResponseMessage responseMessage;

        public FunctionWelcomeTest(FunctionTestFixture fixture)
        {
            testFixture = fixture;
        }

        [Fact]
        public async Task TestFunctionisInvoked()
        {
            responseMessage = await testFixture.Client.GetAsync("api/Welcome?name=Michel");
            Assert.True(responseMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task TestResponseEnd()
        {
            responseMessage = await testFixture.Client.GetAsync("api/Welcome?name=Michel");
            Assert.EndsWith("successfully", await responseMessage.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task TestResponseEqual()
        {
            responseMessage = await testFixture.Client.GetAsync("api/Welcome?name=Michel");
            Assert.Equal("successfully", await responseMessage.Content.ReadAsStringAsync());
        }
    }
}
