﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Function.Integrations
{
    public class Settings
    {
        public string DotnetExPath { set; get; }
        public string FunctionHostPath { set; get; }
        public string FunctionApplicationPath { set; get; }
        public string AzureWebJobsStorage { set; get; }

    }
}
